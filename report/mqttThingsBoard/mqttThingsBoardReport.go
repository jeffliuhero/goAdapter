package mqttThingsBoard

import (
	"encoding/json"
	"goAdapter/device"
	"goAdapter/setting"
	"time"
)

type MQTTThingsBoardReportPropertyTemplate struct {
	DeviceType string //设备类型，"gw" "node"
	DeviceName []string
}

type MQTTThingsBoardPropertyPostParamPropertyTemplate struct {
}

type MQTTThingsBoardPropertyPostParamTemplate struct {
}

type MQTTThingsBoardPropertyPostTemplate struct {
}

type MQTTThingsBoardGWPropertyPostTemplate struct {
}

func MQTTThingsBoardPropertyPost(gwParam ReportServiceGWParamThingsBoardTemplate, propertyParam []MQTTThingsBoardPropertyPostParamTemplate) (int, bool) {

	return 0, true
}

func (r *ReportServiceParamThingsBoardTemplate) GWPropertyPost() {

	properties := make(map[string]interface{})

	properties["MemTotal"] = setting.SystemState.MemTotal

	properties["MemUse"] = setting.SystemState.MemUse

	properties["DiskTotal"] = setting.SystemState.DiskTotal

	properties["DiskUse"] = setting.SystemState.DiskUse

	//properties["Name"] = setting.SystemState.Name

	properties["SN"] = setting.SystemState.SN

	//properties["HardVer"] = setting.SystemState.HardVer

	properties["SoftVer"] = setting.SystemState.SoftVer

	properties["SystemRTC"] = setting.SystemState.SystemRTC

	properties["RunTime"] = setting.SystemState.RunTime

	properties["DeviceOnline"] = setting.SystemState.DeviceOnline

	properties["DevicePacketLoss"] = setting.SystemState.DevicePacketLoss

	//清空接收缓存
	for i := 0; i < len(r.ReceiveReportPropertyAckFrameChan); i++ {
		<-r.ReceiveReportPropertyAckFrameChan
	}

	sJson, _ := json.Marshal(properties)
	propertyPostTopic := "v1/devices/me/telemetry"

	setting.ZAPS.Infof("上报服务[%s]发布上报消息主题%s", r.GWParam.ServiceName, propertyPostTopic)
	if r.GWParam.MQTTClient != nil {
		if token := r.GWParam.MQTTClient.Publish(propertyPostTopic, 0, false, sJson); token.WaitTimeout(2000*time.Millisecond) && token.Error() != nil {
			r.GWParam.ReportErrCnt++
			setting.ZAPS.Debugf("上报服务[%s]上报网关属性失败 失败计数%d/3 ", r.GWParam.ServiceName, r.GWParam.ReportErrCnt)
			if r.GWParam.ReportErrCnt >= 3 {
				r.GWParam.ReportErrCnt = 0

				r.GWParam.ReportStatus = "offLine"
				setting.ZAPS.Warnf("上报服务[%s] 网关离线", r.GWParam.ServiceName)
				//r.GWParam.MQTTClient.Disconnect(0)

				if r.GWLogin() == true {
					r.GWParam.ReportStatus = "onLine"
					setting.ZAPS.Debugf("上报服务[%s] 网关 重新登录成功", r.GWParam.ServiceName)
				} else {
					setting.ZAPS.Warnf("上报服务[%s] 网关 重新登录失败", r.GWParam.ServiceName)
				}
			}
		} else {
			r.GWParam.ReportErrCnt = 0
			setting.ZAPS.Debugf("上报服务[%s]上报网关属性成功 上报内容%v", r.GWParam.ServiceName, string(sJson))
		}
	}
}

//指定设备上传属性
func (r *ReportServiceParamThingsBoardTemplate) NodePropertyPost(name []string) {

	nodesProperties := make(map[string]interface{})
	for _, n := range name {
		for k, v := range r.NodeList {
			if n == v.Name {
				//上报故障计数值先加，收到正确回应后清0
				r.NodeList[k].ReportErrCnt++
				coll, ok := device.CollectInterfaceMap.Coll[v.CollInterfaceName]
				if !ok {
					continue
				}
				for _, d := range coll.DeviceNodeMap {
					if d.Name == v.Name {
						nodesProperty := make(map[string]interface{})
						for _, p := range d.Properties {
							if len(p.Value) >= 1 {
								index := len(p.Value) - 1
								nodesProperty[p.Name] = p.Value[index].Value
							}
						}
						nodesProperties[v.Param.ClientID] = nodesProperty
					}
				}
			}
		}
	}

	//清空接收缓存
	for i := 0; i < len(r.ReceiveReportPropertyAckFrameChan); i++ {
		<-r.ReceiveReportPropertyAckFrameChan
	}

	sJson, _ := json.Marshal(nodesProperties)
	propertyPostTopic := "v1/gateway/attributes"

	setting.ZAPS.Infof("上报服务[%s]发布上报消息主题%s", r.GWParam.ServiceName, propertyPostTopic)
	if r.GWParam.MQTTClient != nil {
		if token := r.GWParam.MQTTClient.Publish(propertyPostTopic, 0, false, sJson); token.WaitTimeout(2000*time.Millisecond) && token.Error() != nil {
			setting.ZAPS.Debugf("上报服务[%s]上报设备属性失败", r.GWParam.ServiceName)
		} else {
			setting.ZAPS.Debugf("上报服务[%s]上报设备属性成功 上报内容%v", r.GWParam.ServiceName, string(sJson))
		}
	}
}
