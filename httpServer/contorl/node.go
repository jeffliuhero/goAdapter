package contorl

import (
	"encoding/json"
	"fmt"
	"goAdapter/device"
	"goAdapter/httpServer/model"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

func ApiAddNode(context *gin.Context) {

	aParam := model.Response{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)
	fmt.Println(string(bodyBuf[:n]))

	nodeInfo := &struct {
		InterfaceName string `json:"CollInterfaceName"`
		DAddr         string `json:"Addr"`
		DType         string `json:"Type"`
		DName         string `json:"Name"`
	}{}

	err := json.Unmarshal(bodyBuf[:n], nodeInfo)
	if err != nil {
		fmt.Println("nodeInfo json unMarshall err,", err)

		aParam.Code = "1"
		aParam.Message = "json unMarshall err"

		context.JSON(http.StatusOK, aParam)
		return
	}

	for _, v := range device.CollectInterfaceMap.Coll {
		for _, d := range v.DeviceNodeMap {
			if nodeInfo.DName == d.Name {
				aParam.Code = "1"
				aParam.Data = ""
				aParam.Message = "设备名称已经存在"
				sJson, _ := json.Marshal(aParam)
				context.String(http.StatusOK, string(sJson))
				return
			}
		}
	}

	device.CollectInterfaceMap.Lock.Lock()
	coll, ok := device.CollectInterfaceMap.Coll[nodeInfo.InterfaceName]
	device.CollectInterfaceMap.Lock.Unlock()
	if !ok {
		aParam.Code = "1"
		aParam.Data = ""
		aParam.Message = "interfaceName is not exist"
		context.JSON(http.StatusOK, aParam)
		return
	}

	_, aParam.Message = coll.AddDeviceNode(nodeInfo.DName, nodeInfo.DType, nodeInfo.DAddr)
	device.WriteCollectInterfaceManageToJson()

	aParam.Code = "0"
	aParam.Data = ""

	context.JSON(http.StatusOK, aParam)

}

func ApiModifyNode(context *gin.Context) {

	type DeleteAck struct {
		Name   string
		Status bool
	}

	aParam := struct {
		Code    string      `json:"Code"`
		Message string      `json:"Message"`
		Data    []DeleteAck `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    make([]DeleteAck, 0),
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)
	log.Println(string(bodyBuf[:n]))

	nodeInfo := &struct {
		InterfaceName string `json:"CollInterfaceName"`
		Name          string `json:"Name"`
		DType         string `json:"Type"`
		Addr          string `json:"Addr"`
	}{
		InterfaceName: "",
		DType:         "",
		Name:          "",
		Addr:          "",
	}

	err := json.Unmarshal(bodyBuf[:n], nodeInfo)
	if err != nil {
		fmt.Println("nodeInfo json unMarshall err,", err)

		aParam.Code = "1"
		aParam.Message = "json unMarshall err"

		context.JSON(http.StatusOK, aParam)
		return
	}

	device.CollectInterfaceMap.Lock.Lock()
	coll, ok := device.CollectInterfaceMap.Coll[nodeInfo.InterfaceName]
	device.CollectInterfaceMap.Lock.Unlock()
	if !ok {
		aParam.Code = "1"
		aParam.Message = "name is not exist"
		context.JSON(http.StatusOK, aParam)
		return
	}

	for _, v := range coll.DeviceNodeMap {
		if v.Name == nodeInfo.Name {
			v.Type = nodeInfo.DType
			v.Addr = nodeInfo.Addr
			device.WriteCollectInterfaceManageToJson()

			aParam.Code = "0"
			aParam.Message = ""
			sJson, _ := json.Marshal(aParam)
			context.String(http.StatusOK, string(sJson))
			return
		}
	}
}

func ApiModifyNodes(context *gin.Context) {

	type DeleteAck struct {
		Name   string
		Status bool
	}

	aParam := struct {
		Code    string      `json:"Code"`
		Message string      `json:"Message"`
		Data    []DeleteAck `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    make([]DeleteAck, 0),
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)
	log.Println(string(bodyBuf[:n]))

	nodeInfo := &struct {
		InterfaceName string   `json:"CollInterfaceName"`
		DType         string   `json:"Type"`
		Name          []string `json:"Name"`
	}{
		InterfaceName: "",
		DType:         "",
		Name:          make([]string, 0),
	}

	err := json.Unmarshal(bodyBuf[:n], nodeInfo)
	if err != nil {
		fmt.Println("nodeInfo json unMarshall err,", err)

		aParam.Code = "1"
		aParam.Message = "json unMarshall err"

		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	for _, v := range nodeInfo.Name {
		ack := DeleteAck{
			Name:   v,
			Status: false,
		}
		aParam.Data = append(aParam.Data, ack)
	}

	for k, v := range nodeInfo.Name {
		device.CollectInterfaceMap.Lock.Lock()
		coll, ok := device.CollectInterfaceMap.Coll[nodeInfo.InterfaceName]
		device.CollectInterfaceMap.Lock.Unlock()
		if !ok {
			continue
		}

		for _, d := range coll.DeviceNodeMap {
			if d.Name == v {
				d.Type = nodeInfo.DType
				device.WriteCollectInterfaceManageToJson()
				aParam.Data[k].Status = true
			}
		}
	}

	aParam.Code = "0"
	aParam.Message = ""
	sJson, _ := json.Marshal(aParam)
	context.String(http.StatusOK, string(sJson))
}

func ApiGetNode(context *gin.Context) {

	sName := context.Query("CollInterfaceName")
	sAddr := context.Query("Addr")

	aParam := &struct {
		Code    string
		Message string
		Data    device.DeviceNodeTemplate
	}{}

	coll, ok := device.CollectInterfaceMap.Coll[sName]
	if !ok {
		aParam.Code = "1"
		aParam.Message = "CollInterfaceName is no exist"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	for _, n := range coll.DeviceNodeMap {
		if n.Addr == sAddr {
			aParam.Code = "0"
			aParam.Message = ""
			aParam.Data = *coll.GetDeviceNode(sAddr)
			sJson, _ := json.Marshal(aParam)
			context.String(http.StatusOK, string(sJson))
			return
		}
	}
}

func ApiDeleteNode(context *gin.Context) {

	type DeleteAck struct {
		Name   string
		Status bool
	}

	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    []DeleteAck
	}{
		Code:    "1",
		Message: "",
		Data:    make([]DeleteAck, 0),
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)
	fmt.Println(string(bodyBuf[:n]))

	nodeInfo := &struct {
		InterfaceName string   `json:"CollInterfaceName"`
		DName         []string `json:"Name"`
	}{
		InterfaceName: "",
		DName:         make([]string, 0),
	}

	err := json.Unmarshal(bodyBuf[:n], nodeInfo)
	if err != nil {
		fmt.Println("nodeInfo json unMarshall err,", err)

		aParam.Code = "1"
		aParam.Message = "json unMarshall err"

		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	for _, v := range nodeInfo.DName {

		ack := DeleteAck{
			Name:   v,
			Status: false,
		}
		aParam.Data = append(aParam.Data, ack)
	}

	for k, DName := range nodeInfo.DName {
		device.CollectInterfaceMap.Lock.Lock()
		coll, ok := device.CollectInterfaceMap.Coll[nodeInfo.InterfaceName]
		device.CollectInterfaceMap.Lock.Unlock()
		if !ok {
			continue
		}

		for _, n := range coll.DeviceNodeMap {
			if n.Name == DName {
				coll.DeleteDeviceNode(DName)
				device.WriteCollectInterfaceManageToJson()
				if len(nodeInfo.DName) > 1 {
					nodeInfo.DName = append(nodeInfo.DName[:k], nodeInfo.DName[k+1:]...)
				}
				aParam.Data[k].Status = true
				continue
			}
		}
	}

	aParam.Code = "0"
	aParam.Message = ""

	sJson, _ := json.Marshal(aParam)
	context.String(http.StatusOK, string(sJson))
}

/**
从缓存中获取设备变量
*/
func ApiGetNodeVariableFromCache(context *gin.Context) {

	type VariableTemplate struct {
		Index     int         `json:"index"` // 变量偏移量
		Name      string      `json:"name"`  // 变量名
		Label     string      `json:"lable"` // 变量标签
		Value     interface{} `json:"value"` // 变量值
		Explain   interface{} `json:"explain"`
		TimeStamp string      `json:"timestamp"` // 变量时间戳
		Type      string      `json:"type"`      // 变量类型
	}

	sName := context.Query("CollInterfaceName")
	sAddr := context.Query("Addr")

	aParam := &struct {
		Code    string
		Message string
		Data    []VariableTemplate
	}{}

	coll, ok := device.CollectInterfaceMap.Coll[sName]
	if !ok {
		aParam.Code = "1"
		aParam.Message = "node is noExist"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	for _, d := range coll.DeviceNodeMap {
		if d.Addr == sAddr {
			aParam.Code = "0"
			aParam.Message = ""
			aParam.Data = make([]VariableTemplate, 0)
			index := 0
			variable := VariableTemplate{}
			for _, p := range d.Properties {
				variable.Name = p.Name
				variable.Label = p.Explain
				if p.Type == device.PropertyTypeUInt32 {
					variable.Type = "uint32"
				} else if p.Type == device.PropertyTypeInt32 {
					variable.Type = "int32"
				} else if p.Type == device.PropertyTypeDouble {
					variable.Type = "double"
				} else if p.Type == device.PropertyTypeString {
					variable.Type = "string"
				}
				// 取出切片中最后一个值
				if len(p.Value) > 0 {
					index = len(p.Value) - 1
					variable.Index = p.Value[index].Index
					variable.Value = p.Value[index].Value
					variable.Explain = p.Value[index].Explain
					variable.TimeStamp = p.Value[index].TimeStamp
				} else {
					variable.Value = ""
					variable.Explain = ""
					variable.TimeStamp = ""
				}
				aParam.Data = append(aParam.Data, variable)
			}

			sJson, _ := json.Marshal(aParam)
			context.String(http.StatusOK, string(sJson))
			return
		}
	}

}

func ApiGetNodeHistoryVariableFromCache(context *gin.Context) {

	sName := context.Query("CollInterfaceName")
	sAddr := context.Query("Addr")
	sVariable := context.Query("VariableName")

	aParam := &struct {
		Code    string
		Message string
		Data    []device.DeviceTSLPropertyValueTemplate
	}{}

	coll, ok := device.CollectInterfaceMap.Coll[sName]
	if !ok {
		aParam.Code = "1"
		aParam.Message = "node is no exist"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))

		return
	}

	for _, d := range coll.DeviceNodeMap {
		if d.Addr == sAddr {
			aParam.Code = "0"
			aParam.Message = ""
			for _, v := range d.Properties {
				if v.Name == sVariable {
					aParam.Data = append(aParam.Data, v.Value...)
				}
			}

			sJson, _ := json.Marshal(aParam)
			context.String(http.StatusOK, string(sJson))

			return
		}
	}
}

func ApiGetNodeReadVariable(context *gin.Context) {

	type VariableTemplate struct {
		Index     int         `json:"index"` // 变量偏移量
		Name      string      `json:"name"`  // 变量名
		Label     string      `json:"lable"` // 变量标签
		Value     interface{} `json:"value"` // 变量值
		Explain   interface{} `json:"explain"`
		TimeStamp string      `json:"timestamp"` // 变量时间戳
		Type      string      `json:"type"`      // 变量类型
	}

	sName := context.Query("CollInterfaceName")
	sAddr := context.Query("Addr")

	aParam := &struct {
		Code    string
		Message string
		Data    []VariableTemplate
	}{}

	//查找设备是否存在
	device.CollectInterfaceMap.Lock.Lock()
	coll, ok := device.CollectInterfaceMap.Coll[sName]
	device.CollectInterfaceMap.Lock.Unlock()
	if !ok {
		aParam.Code = "1"
		aParam.Message = "node is no exist"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	nodeName := ""
	for _, v := range coll.DeviceNodeMap {
		if v.Addr == sAddr {
			nodeName = v.Name
		}
	}
	if nodeName == "" {
		aParam.Code = "1"
		aParam.Message = "node is no exist"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	//发送命令到响应的采集接口
	cmd := device.CommunicationCmdTemplate{}
	cmd.CollInterfaceName = sName
	cmd.DeviceName = coll.DeviceNodeMap[nodeName].Name
	cmd.FunName = "GetRealVariables"
	cmd.FunPara = ""
	cmdRX := coll.CommQueueManage.CommunicationManageAddEmergency(cmd)
	if cmdRX.Status == true {
		aParam.Code = "0"
		aParam.Message = ""
		aParam.Data = make([]VariableTemplate, 0)
		index := 0
		variable := VariableTemplate{}
		for _, v := range coll.DeviceNodeMap[nodeName].Properties {
			//variable.Index = v.Index
			variable.Name = v.Name
			variable.Label = v.Explain
			// 取出切片中最后一个值
			if len(v.Value) > 0 {
				index = len(v.Value) - 1
				variable.Value = v.Value[index].Value
				variable.Explain = v.Value[index].Explain
				variable.TimeStamp = v.Value[index].TimeStamp
			} else {
				variable.Value = ""
				variable.Explain = ""
				variable.TimeStamp = ""
			}
			//variable.Type = v.Type
			aParam.Data = append(aParam.Data, variable)
		}

		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
	} else {
		aParam.Code = "1"
		aParam.Message = "device is not return"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
	}
}

func ApiInvokeService(context *gin.Context) {

	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)

	serviceInfo := struct {
		CollInterfaceName string
		DeviceName        string
		ServiceName       string
		ServiceParam      map[string]interface{}
	}{}
	err := json.Unmarshal(bodyBuf[:n], &serviceInfo)
	if err != nil {
		fmt.Println("serviceInfo json unMarshall err,", err)

		aParam.Code = "1"
		aParam.Message = "json unMarshall err"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	device.CollectInterfaceMap.Lock.Lock()
	coll, ok := device.CollectInterfaceMap.Coll[serviceInfo.CollInterfaceName]
	device.CollectInterfaceMap.Lock.Unlock()
	if !ok {
		aParam.Code = "1"
		aParam.Message = "device is not exist"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	cmd := device.CommunicationCmdTemplate{}
	cmd.CollInterfaceName = serviceInfo.CollInterfaceName
	cmd.DeviceName = serviceInfo.DeviceName
	cmd.FunName = serviceInfo.ServiceName
	paramStr, _ := json.Marshal(serviceInfo.ServiceParam)
	cmd.FunPara = string(paramStr)
	cmdRX := coll.CommQueueManage.CommunicationManageAddEmergency(cmd)
	if cmdRX.Status == true {
		aParam.Code = "0"
		aParam.Message = ""
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
	} else {
		aParam.Code = "1"
		aParam.Message = "device is not return"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
	}
}
