package setting

import (
	"goAdapter/utils"
)

func BackupFiles() (bool, string) {
	utils.DirIsExist("./tmp")
	dirMap := []string{"./plugin", "./selfpara"}

	zipFileName := "./tmp/backup_" + SystemState.SN + ".zip"

	_ = utils.CompressDirsToZip(dirMap, zipFileName)

	return true, zipFileName
}
